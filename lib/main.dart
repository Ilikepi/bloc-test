import 'dart:async';

import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

/// Custom [BlocObserver] which observes all bloc and cubit instances.
class SimpleBlocObserver extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object event) {
    print(event);
    super.onEvent(bloc, event);
  }

  @override
  void onChange(Cubit cubit, Change change) {
    print(change);
    super.onChange(cubit, change);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    print(transition);
    super.onTransition(bloc, transition);
  }

  @override
  void onError(Cubit cubit, Object error, StackTrace stackTrace) {
    print(error);
    super.onError(cubit, error, stackTrace);
  }
}

void main() {
  Bloc.observer = SimpleBlocObserver();
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocProvider(
        create: (_) => CounterBloc(),
        child: CounterPage(),
      ),
    );
  }
}

class CounterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Counter')),
      body: BlocBuilder<CounterBloc, State>(
        builder: (_, count) {
          if (count is LoadingState) {
            return Center(child: CircularProgressIndicator());
          }
          if (count is NumberState)
            return Center(
              child: Text('${count.i}',
                  style: Theme.of(context).textTheme.headline1),
            );

          if (count is ErrorState) return Center(child: Text(count.message));

          return Container();
        },
      ),
      floatingActionButton: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: FloatingActionButton(
              child: const Icon(Icons.add),
              onPressed: () =>
                  context.read<CounterBloc>().add(CounterEvent.increment),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: FloatingActionButton(
              child: const Icon(Icons.remove),
              onPressed: () =>
                  context.read<CounterBloc>().add(CounterEvent.decrement),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: FloatingActionButton(
              child: const Icon(Icons.account_tree_outlined),
              onPressed: () => context
                  .read<CounterBloc>()
                  .add(CounterEvent.decrementAndIncrementTwice),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: FloatingActionButton(
              backgroundColor: Colors.red,
              child: const Icon(Icons.error),
              onPressed: () => context.read<CounterBloc>().add(null),
            ),
          ),
        ],
      ),
    );
  }
}

class State {}

class LoadingState extends State {}

class ErrorState extends State {
  final String message;
  final int code;

  ErrorState({@required this.message, @required this.code});
}

class NumberState extends State {
  final int i;

  NumberState(this.i);
}

enum CounterEvent { increment, decrement, decrementAndIncrementTwice }

class CounterBloc extends Bloc<CounterEvent, State> {
  CounterBloc() : super(NumberState(0));

  errorState({String message, int code}) =>
      ErrorState(message: message, code: code);

  Stream<State> _getStateFromFuture(Future<State> future) async* {
    yield LoadingState();
    try {
      State state = await future;

      yield state;
    } catch (err) {
      yield ErrorState(message: "Something went wrong", code: 100);
    }
  }

// Stream
  Stream<State> Function(State) _getStateFromFutures(
          List<Future<State> Function(State)> futures) =>
      (State initialState) async* {
        // we initially yield a loading state
        yield LoadingState();

        State state = initialState;

        for (var i = 0; i < futures.length; i++) {
          try {
            state = await futures[i](state);
            yield state;
          } catch (err) {
            yield ErrorState(message: "Something went wrong", code: 100);
          }
        }
      };

  // Future<State> Function(State)

  Future<State> _decrementNumber(State previousState) async {
    // throw "Hi";
    int previousCount =
        !(previousState is NumberState) ? 0 : (previousState as NumberState).i;
    return Future.delayed(Duration(seconds: 1), () {
      int newNumber = previousCount - 1;

      return NumberState(newNumber);
    });
  }

  /************************* */

  Future<State> _incrementNumber(State previousState) async {
    int previousCount =
        !(previousState is NumberState) ? 0 : (previousState as NumberState).i;

    NumberState numberState = await Future.delayed(Duration(seconds: 1), () {
      int newNumber = previousCount + 1;

      return NumberState(newNumber);
    });

    print(numberState);

    return numberState;
  }

  /************************* */

  // Ideally, what we want to do here is exactly what a stream does
  //_decrement the number, show the decremented number, increment again
  // show the incremented number, and then increment again and show
  Stream<State> _decrementAndThenIncrementTwice(State state) {
    return _getStateFromFutures(
        [_decrementNumber, _incrementNumber, _incrementNumber])(state);
  }

  @override
  Stream<State> mapEventToState(CounterEvent event) {
    switch (event) {
      case CounterEvent.decrement:
        return _getStateFromFuture(_decrementNumber(state));
        break;
      case CounterEvent.increment:
        return _getStateFromFuture(_incrementNumber(state));
        break;
      case CounterEvent.decrementAndIncrementTwice:
        return _decrementAndThenIncrementTwice(state);
        break;
      default:
        addError(Exception('unsupported event'));
    }
  }
}

// NB:
// another consideration would be is that if we are using this framework,
// we could possible just have a mapping that works over mapEventToState:
// such that:

// Map<CounterEvent, dynamic> _map = {

//   CounterEvent.decrement: _getStateFromFuture(_decrementNumber(state)),
//   CounterEvent.increment: _getStateFromFuture(_incrementNumber(state)),
//   CounterEvent.decrementAndIncrementTwice:_decrementAndThenIncrementTwice()
// }

// methods would obviously change based on inputs
